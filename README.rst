This is a collection of Kwant scripts that have been disseminated as
examples of good Kwant usage that are meant to represent a wide
spectrum of Kwant usage.  The scripts are original, but may have been
slightly modified for faster execution (e.g. reducing system sizes and
numbers of iterations).  Over time, the scripts may be modified to
adapt to backwards-incompatible changes in Kwant.

The main purpose of this collection is to guard against accidental breaking of
backwards compatibility.  In Kwant, we do not exclude breaking backwards
compatibility, but doing so should be deliberate and not accidental.

The script canon is complementary to unit tests: the latter focus on
correctness (even of corner cases) and are typically updated to the
most modern API usage.  The canon, on the other hand, is static and
concentrates on typical and exemplary usage.

This shell script 'validate' may be used to run all the scripts in
parallel.
