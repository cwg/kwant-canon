import numpy as np
import kwant
from matplotlib import pyplot


lat = kwant.lattice.square()


def make_lead_x(W=10, t=1):
    syst = kwant.Builder(kwant.TranslationalSymmetry([-1, 0]))
    syst[(lat(0, y) for y in range(W))] = 4 * t
    syst[lat.neighbors()] = -t
    return syst


_lead = make_lead_x()
kwant.plot(_lead, show=False)
kwant.plotter.bands(_lead.finalized(), show=False)


def make_periodic_lead(W=10, t=1):
    syst = make_lead_x(W, t)
    #### Add periodic boundary conditions here.
    return syst


_lead = make_periodic_lead()
kwant.plotter.bands(_lead.finalized(), show=False)


def make_wire(W=10, L=30, t=1):
    # Construct the scattering region.
    sr = kwant.Builder()
    sr[(lat(x, y) for x in range(L) for y in range(W))] = 4 * t
    sr[lat.neighbors()] = -t

    # Build and attach lead from both sides.
    lead = make_lead_x(W, t)
    sr.attach_lead(lead)
    sr.attach_lead(lead.reversed())

    return sr


def plot_transmission(syst, energies):
    # Compute conductance
    trans = []
    refl = []
    for energy in energies:
        smatrix = kwant.smatrix(syst, energy)
        trans.append(smatrix.transmission(1, 0))
        refl.append(smatrix.transmission(0, 0))
    pyplot.plot(energies, trans, '.')
    pyplot.plot(energies, refl, 'o')


_wire = make_wire()
kwant.plot(_wire, show=False)
plot_transmission(_wire.finalized(), np.linspace(0, 2, 101))


def make_dot(center, radius, t=1):
    def is_inside(pos):
        x, y = pos - center
        return x**2 + y**2 < rr

    rr = radius**2
    syst = kwant.Builder()
    syst[lat.shape(is_inside, center)] = 4 * t
    syst[lat.neighbors()] = -t

    return syst


_dot = make_dot((3, 7), 13)
_dot.attach_lead(make_lead_x(10))
kwant.plot(_dot, show=False)
