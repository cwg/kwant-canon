import kwant
import matplotlib.pyplot as plt
import scipy.linalg as la


lat = kwant.lattice.square(a=1)


t = 1
r = 15

def circle(pos):
    x, y = pos
    return x**2 + y**2 < r**2

sys = kwant.Builder()
sys[lat.shape(circle, (0,0))] = 4 * t
sys[lat.neighbors()] = -t


kwant.plot(sys, show=False)


sys = sys.finalized()


ham = sys.hamiltonian_submatrix()
plt.matshow(ham==0, cmap="gray", interpolation=None)


eval, evec = la.eigh(ham)

kwant.plotter.map(sys, abs(evec[:,0])**2, show=False)


def plot_wf(i=0):
    print("Plotting wave function with index", i)
    print("energy:", eval[i],"x t")
    kwant.plotter.map(sys, abs(evec[:, i])**2, show=False)

plot_wf(15)
